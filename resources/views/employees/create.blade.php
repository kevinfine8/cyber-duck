@extends('adminlte::page')
@section('title', 'Add Employee')
@section('content_header')
<h1>Add Employee</h1>
@stop
@section('content')
<div class="row">
	<div class="col-sm-8">
		<div>
			@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div><br />
			@endif
			<form method="post" action="/employees/" enctype="multipart/form-data">
				@csrf
				<div class="form-group">    
					<label for="name">First name:</label>
					<input type="text" class="form-control" name="first_name"/>
				</div>
				<div class="form-group">    
					<label for="name">Last name:</label>
					<input type="text" class="form-control" name="last_name"/>
				</div>
				<div class="form-group">
					<label for="email">Email:</label>
					<input type="text" class="form-control" name="email"/>
				</div> 
				<div class="form-group">
					<label for="phone">Phone:</label>
					<input type="text" class="form-control" name="phone" />
				</div>
				<div class="form-group">
					<label for="cars">company</label>
					<select name='company' class="form-control">
						@foreach($companies as $company)
						<option  value="{{$company->id}}">{{$company->name}}</option>
						@endforeach
					</select>
				</div>

				<button type="submit" class="btn btn-primary">Add</button>
			</form>
		</div>
	</div>
</div>
@stop