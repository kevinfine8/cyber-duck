@extends('adminlte::page')
@section('title', 'Employees')
@section('content_header')
<h1>Employees<a href="/employees/create" type="button" class="ml-4 btn btn-primary">Add Employee</a></h1>
@stop
@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="box ml-2">
			<div class="box-header">
				@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
				@endif     
			</div>
			<div class="box-body">
				<table id="laravel_datatable" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>Id</th>
							<th>Name</th>
							<th>Email</th>
							<th colspan = 2>Actions</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($employees as $employee)
						<tr>
							<td>{{ $employee->id }}</td>
							<td>{{ $employee->first_name }} {{ $employee->last_name }}</td>
							<td>{{ $employee->email }}</td>
							<td>
								<a href="{{ route('employees.edit',$employee->id)}}" class="btn btn-primary">Edit</a>
							</td>
							<td>
								<form action="{{ route('employees.destroy', $employee->id)}}" method="post">
									@csrf
									@method('DELETE')
									<button class="btn btn-danger" type="submit">Delete</button>
								</form>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				{{ $employees->links() }}
			</div>
		</div>
	</div>
</div>
@stop