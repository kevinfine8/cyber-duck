@extends('adminlte::page')
@section('title', 'Edit employee')
@section('content_header')
<h1>Edit employee</h1>
@stop
@section('content')
<div class="row">
	<div class="col-sm-8">
		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div><br />
		@endif
		<form method="post" action="/employees/{{ $employee->id }}" enctype="multipart/form-data">
			@method('PATCH') 
			@csrf
			<div class="form-group">    
				<label for="name">First name:</label>
				<input type="text" class="form-control" name="first_name" value={{ $employee->first_name }} />
			</div>
			<div class="form-group">    
				<label for="name">Last name:</label>
				<input type="text" class="form-control" name="last_name" value={{ $employee->last_name }} />
			</div>
			<div class="form-group">
				<label for="email">Email:</label>
				<input type="text" class="form-control" name="email" value={{ $employee->email }} />
			</div> 
			<div class="form-group">
				<label for="phone">Phone:</label>
				<input type="text" class="form-control" name="phone" value={{ $employee->phone }}  />
			</div>
			<div class="form-group">
				<label for="cars">company</label>
				<select name='company' class="form-control">
					@foreach($companies as $company)
					<option  value="{{$company->id}}" {{ ( $company->id == $employee->company) ? 'selected' : '' }}selected="selected" >{{$company->name}}</option>
					@endforeach
				</select>
			</div>
			<button type="submit" class="btn btn-primary">Update</button>
		</form>
	</div>
</div>
@stop