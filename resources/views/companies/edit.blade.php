@extends('adminlte::page')
@section('title', 'Update company')
@section('content_header')
<h1>Update company</h1>
@stop
@section('content')
<div class="row">
	<div class="col-sm-8">
		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div><br />
		@endif
		<form method="post" action="/companies/{{ $company->id }}" enctype="multipart/form-data">
			@method('PATCH') 
			@csrf
			<div class="form-group">    
				<label for="name">Name:</label>
				<input type="text" class="form-control" name="name" value={{ $company->name }} />
			</div>
			<div class="form-group">
				<label for="email">Email:</label>
				<input type="text" class="form-control" name="email" value={{ $company->email }} />
			</div>
			<div class="form-group">
				<label for="logo">Logo:</label>
				<div class="custom-file">
					<input type="file" class="custom-file-input" name="logo" >
					<label class="custom-file-label" for="exampleInputFile">Choose file</label>
				</div>
				<div class="input-group-append">
					<span class="input-group-text" id="">Upload</span>
				</div>
				<img src="{{ asset('storage/'.$company->logo) }}"> 
			</div>                    
			<button type="submit" class="btn btn-primary">Update</button>
		</form>
	</div>
</div>
@stop