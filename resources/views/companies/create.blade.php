@extends('adminlte::page')
@section('title', 'Companies')
@section('content_header')
<h1>Add a Company</h1>
@stop
@section('content')
	<div class="row">
		<div class="col-sm-8">
			<div>
				@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div><br />
				@endif
				<form method="post" action="/companies/" enctype="multipart/form-data">
					@csrf
					<div class="form-group">    
						<label for="name">Name:</label>
						<input type="text" class="form-control" name="name"/>
					</div>
					<div class="form-group">
						<label for="email">Email:</label>
						<input type="text" class="form-control" name="email"/>
					</div>
					<div class="form-group">
						<label for="logo">Logo</label>
						<div class="custom-file">
							<input type="file" class="custom-file-input" name="logo" >
							<label class="custom-file-label" for="exampleInputFile">Choose file</label>
						</div>
						<div class="input-group-append">
							<span class="input-group-text" id="">Upload</span>
						</div>
					</div>
					<button type="submit" class="btn btn-primary">Add</button>
				</form>
			</div>
		</div>
	</div>
@stop