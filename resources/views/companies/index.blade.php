@extends('adminlte::page')
@section('title', 'Companies')
@section('content_header')
<h1>Companies<a href="/companies/create" type="button" class="ml-4 btn btn-primary">Add Company</a></h1>
@stop
@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
				@endif     
			</div>
			<div class="box-body">
				<table id="laravel_datatable" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>Id</th>
							<th>Name</th>
							<th>Email</th>
							<th colspan = 2>Actions</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($companies as $company)
						<tr>
							<td>{{ $company->id }}</td>
							<td>{{ $company->name }}</td>
							<td>{{ $company->email }}</td>
							<td>
								<a href="{{ route('companies.edit',$company->id)}}" class="btn btn-primary">Edit</a>
							</td>
							<td>
								<form action="{{ route('companies.destroy', $company->id)}}" method="post">
									@csrf
									@method('DELETE')
									<button class="btn btn-danger" type="submit">Delete</button>
								</form>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				{{ $companies->links() }}
			</div>
		</div>
	</div>
</div>
@stop